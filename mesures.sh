#!/bin/bash

WIDTH=$1
HEIGHT=$2
STEPS=$3
EXECUTIONS=$4

MAX_THREADS=512

echo -e "---\\nSéquenciel en univers borné :"
for ((i=1; i <= EXECUTIONS ; i++))
do
	echo -en "$i:\\t"
	./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b b -m s -v /dev/null -o /dev/null
done

echo -e "---\\nSéquenciel en univers continu :"
for ((i=1; i <= EXECUTIONS ; i++))
do
	echo -en "$i:\\t"
	./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b c -m s -v /dev/null -o /dev/null
done

echo -e "---\\nMultithread en univers borné :"
for ((i=1; i <= EXECUTIONS ; i++))
do
	echo -en "$i:\\t"
	./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b b -m m -v /dev/null -o /dev/null
done

echo -e "---\\nMultithread en univers continu :"
for ((i=1; i <= EXECUTIONS ; i++))
do
	echo -en "$i:\\t"
	./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b c -m m -v /dev/null -o /dev/null
done

echo -e "---\\nMultithread en univers borné, nombre de threads variables :"
for ((i=1; i <= MAX_THREADS ; i*=2))
do
	echo -en "$i:\\t"
	./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b b -m m -t $i -v /dev/null -o /dev/null
done

echo -e "---\\nMultithread en univers continu, nombre de threads variables :"
for ((i=1; i <= MAX_THREADS ; i*=2))
do
	echo -en "$i:\\t"
	./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b c -m m -t $i -v /dev/null -o /dev/null
done

