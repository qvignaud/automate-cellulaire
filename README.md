
# Game of Life / Jeu de la Vie

Cellular automaton done during the winter semester 2017 in UQÀM for the curses of advanced parallel programming.

See documents [subject](dev1-171.pdf) and [report](rapport.pdf) (in french) for informations and documentation.

