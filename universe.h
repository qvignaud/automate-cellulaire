#ifndef __UNIVERSE_H__
#define __UNIVERSE_H__

#include <cstdlib>
#include <vector>
#include <string>

/**
	Énumération des états cellulaires possibles.
*/
enum State : char
{
	///État mort
	Dead = 0,
	///État vivant
	Alive = 1
};

/**
	Énumération des possiblités de limites d'univers.
	Note : un univers est toujours fermé et son espace défini et fixe.
*/
enum UniverseLimit : char
{
	/**
		Univers borné, les limites ne sont pas franchissables et sont blocantes.
	*/
	Bounded = 0,
	/**
		Univers continu, les limites n'existent pas d'un point de vue interne à l'univers. Les bords "bouclent" entre-eux.
	*/
	Continuous = 1
};

/**
	Classe d'univers.
	
	Un univers est décrit par ses dimensions x ("largeur") et y ("hauteur"), ainsi que son type de limite.
	Un univers tout juste créé est rempli de cellules mortes, pour le remplir (aléatoirement), se réferer à la méthode fill_random().
	
	Pour évoluer dans le temps, il est nécessaire d'appeler -au choix- l'une des méthodes evolute(), ce qui préparera l'état futur de l'univers, puis d'appeler la méthode next_step().
*/
class Universe
{
	public:
		/**
			Constructeur.
			Crée un univers vide de dimensions x et y.
		*/
		Universe (unsigned int x, unsigned int y, UniverseLimit limit=UniverseLimit::Bounded);
		~Universe ();
		
		///Dimension x ("largeur") de l'univers.
		inline unsigned int get_x() { return x; };
		///Dimension y ("hauteur") de l'univers.
		inline unsigned int get_y() { return y; };
		///Type de limite de l'univers.
		inline UniverseLimit get_limit() { return limit; };
		
		/**
			Retourne la représentation visuelle de l'univers, pour la sortie console ou fichier texte, depuis son premier instant jusqu'au présent.
		*/
		std::string get_cout_matrix_all();
		/**
			Retourne la représentation visuelle de l'univers, pour la sortie console ou fichier texte, à l'instant présent.
		*/
		std::string get_cout_matrix_present();
		/**
			Retourne la représentation Javascript de l'univers (tableau de 1 ou 0) depuis son premier instant jusqu'au présent.
		*/
		std::string get_js_array_all();
		/**
			Retourne la représentation Javascript de l'univers (tableau de 1 ou 0), à l'instant présent.
		*/
		std::string get_js_array_present();
		
		/**
			Procède au remplissage de l'instant présent par 10% de cellules vivantes réparties aléatoirement d'après la graine donnée.
		*/
		void fill_random(unsigned int seed);
		
		/**
			Procède à l'évolution de l'intégralité de l'univers.
		*/
		void evolute();
		/**
			Procède à l'évolution des cellules contenues dans les intervalles donnés (exclusifs).
		*/
		void evolute(unsigned int x_min, unsigned int x_max, unsigned int y_min, unsigned int y_max);
		/**
			Procède à l'évolution de la cellule indiquée.
		*/
		void evolute(unsigned int x, unsigned int y);
		
		/**
			Procède au passage de l'instant présent à l'instant futur.
		*/
		void next_step();

	private:
		///Largeur de l'univers.
		unsigned int x;
		///Heuteur de l'univers.
		unsigned int y;
		///Type de limite de l'univers.
		UniverseLimit limit;
		
		/**
			Retourne le nombre de cellules voisines directes vivantes (comportement variant selon le type de limite qu'a l'univers).
		*/
		unsigned short int neighbors(unsigned int x, unsigned int y);
		
		/**
			Retourne la représentation visuelle de l'univers, pour la sortie console ou fichier texte, à l'instant spécifié.
		*/
		std::string get_cout_matrix(std::vector < std::vector < State > > *step);
		/**
			Retourne la représentation Javascript de l'univers (tableau de 1 ou 0), à l'instant spécifié.
		*/
		std::string get_js_array(std::vector < std::vector < State > > *step);
	
		/**
			Vecteur contenant les pointeurs ordonnés par ordre chronologique de tous les instants de l'univers depuis son commencement, excepté le présent et le futur.
		*/
		std::vector < std::vector < std::vector < State > >* > history;
		///Pointeur vers l'instant présent.
		std::vector < std::vector < State > > *present = nullptr;
		///Pointeur vers l'instant futur.
		std::vector < std::vector < State > > *future = nullptr;
};

#endif /* __UNIVERSE_H__ */

