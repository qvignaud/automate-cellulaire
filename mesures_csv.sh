#!/bin/bash

WIDTH=$1
HEIGHT=$2
STEPS=$3
EXECUTIONS=$4

OUTPUT_DIR=$5
OUTPUT_FILE=`echo $OUTPUT_DIR/mesures-width$WIDTH-height$HEIGHT-steps$STEPS-executions$EXECUTIONS.csv`

MAX_THREADS=1024

echo -n "Mesures pour univers $WIDTH x $HEIGHT pour $STEPS instants (moyenne de $EXECUTIONS exécutions)... "

echo -en "" > $OUTPUT_FILE

for ((t=1; t <= MAX_THREADS ; t*=2))
do
	echo -n ";$t" >> $OUTPUT_FILE
done

# Séquenciel univers borné
echo -en "\\nsequencial_bounded" >> $OUTPUT_FILE
RESULT=0
for ((i=1; i <= EXECUTIONS ; i++))
do
	OUTPUT=`./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b b -m s -v /dev/null -o /dev/null -x`
	RESULT=`echo $OUTPUT + $RESULT | bc -l`
done
RESULT=`echo $RESULT / $EXECUTIONS | bc -l`
for ((t=1; t <= MAX_THREADS ; t*=2))
do
	echo -n ";$RESULT" >> $OUTPUT_FILE
done

# Séquenciel univers continu
echo -en "\\nsequencial_continuous" >> $OUTPUT_FILE
RESULT=0
for ((i=1; i <= EXECUTIONS ; i++))
do
	OUTPUT=`./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b c -m s -v /dev/null -o /dev/null -x`
	RESULT=`echo $OUTPUT + $RESULT | bc -l`
done
RESULT=`echo $RESULT / $EXECUTIONS | bc -l`
for ((t=1; t <= MAX_THREADS ; t*=2))
do
	echo -n ";$RESULT" >> $OUTPUT_FILE
done

#Multithread univers borné
echo -en "\\nmultithread_bounded" >> $OUTPUT_FILE
for ((t=1; t <= MAX_THREADS ; t*=2))
do
	RESULT=0
	for ((i=1; i <= EXECUTIONS ; i++))
	do
		OUTPUT=`./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b b -m m -t $t -v /dev/null -o /dev/null -x`
		RESULT=`echo $OUTPUT + $RESULT | bc -l`
	done
	RESULT=`echo $RESULT / $EXECUTIONS | bc -l`
	echo -n ";$RESULT" >> $OUTPUT_FILE
done

#Multithread univers continu
echo -en "\\nmultithread_continuous" >> $OUTPUT_FILE
for ((t=1; t <= MAX_THREADS ; t*=2))
do
	RESULT=0
	for ((i=1; i <= EXECUTIONS ; i++))
	do
		OUTPUT=`./automate -w $WIDTH -h $HEIGHT -s $STEPS -d 0 -b c -m m -t $t -v /dev/null -o /dev/null -x`
		RESULT=`echo $OUTPUT + $RESULT | bc -l`
	done
	RESULT=`echo $RESULT / $EXECUTIONS | bc -l`
	echo -n ";$RESULT" >> $OUTPUT_FILE
done

echo "Fait."

