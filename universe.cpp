
#include "universe.h"

Universe::Universe(unsigned int x, unsigned int y, UniverseLimit limit)
	: x(x), y(y), limit(limit) 
{
	present = new std::vector<std::vector<State> > (y, std::vector<State>(x, State::Dead));
	future = new std::vector<std::vector<State> > (*present);
}

Universe::~Universe()
{
	delete present;
	delete future;
	for (unsigned int i=0 ; i < history.size() ; i++) {
		delete history.at(i);
	}
}

std::string Universe::get_cout_matrix_all()
{
	std::string cout_matrix;
	for (unsigned int i=0 ; i < history.size() ; i++) {
		cout_matrix += get_cout_matrix(history.at(i));
	}
	cout_matrix += get_cout_matrix_present();
	return cout_matrix;
}

std::string Universe::get_cout_matrix_present()
{
	return get_cout_matrix(present);
}

std::string Universe::get_js_array_all()
{
	std::string js_array;
	for (unsigned int i=0 ; i < history.size() ; i++) {
		js_array += get_js_array(history.at(i));
	}
	js_array += get_js_array_present();
	return js_array;
}

std::string Universe::get_js_array_present()
{
	return get_js_array(present);
}

std::string Universe::get_cout_matrix(std::vector < std::vector < State > > *step)
{
	std::string output;
	for (unsigned int i=0 ; i < y ; i++) {
		for (unsigned int j=0 ; j < x ; j++) {
			State state = step->at(i).at(j);
			if (state == State::Alive) output += 'X';
			else output += '-';
		}
		output += '\n';
	}
	return output;
}

std::string Universe::get_js_array(std::vector < std::vector < State > > *step)
{
	std::string js_array;
	js_array += '[';
	for (unsigned int i=0 ; i < y ; i++) {
		js_array += '[';
		for (unsigned int j=0 ; j < x ; j++) {
			State state = step->at(i).at(j);
			if (state == State::Alive) js_array += "1,";
			else js_array += "0,";
		}
		js_array += "],";
	}
	js_array += "],";
	return js_array;
}

void Universe::fill_random(unsigned int seed)
{
	srand(seed);
	int level_rand = RAND_MAX/10;
	for (unsigned int i=0 ; i < y ; i++) {
		for (unsigned int j=0 ; j < x ; j++) {
		
			int random_number = rand();
			State state = State::Dead;
			if (random_number < level_rand) state = State::Alive;
			
			present->at(i)[j] = state;
		}
	}
}

void Universe::evolute()
{
	for (unsigned int i=0 ; i < y ; i++) {
		for (unsigned int j=0 ; j < x ; j++) {
			evolute(j, i);
		}
	}
}

void Universe::evolute(unsigned int x_min, unsigned int x_max, unsigned int y_min, unsigned int y_max)
{
	for (unsigned int i=y_min ; i < y_max ; i++) {
		for (unsigned int j=x_min ; j < x_max ; j++) {
			evolute(j, i);
		}
	}
}

void Universe::evolute(unsigned int x, unsigned int y)
{
	State state = present->at(y).at(x);
	unsigned short int count = this->neighbors(x, y);
	if (state == State::Dead && count == 3) future->at(y)[x] = State::Alive;
	else if (state == State::Alive) {
		if (count >= 2 && count <=3) future->at(y)[x] = State::Alive;
		else future->at(y)[x] = State::Dead;
	}
	else future->at(y)[x] = State::Dead;
}

void Universe::next_step()
{
	std::vector < std::vector < State > > *temp_ptr = present;
	
	//Copie du présent pour l'historisation.
	std::vector < std::vector < State > > *temp_history = new std::vector < std::vector < State > >(*present);
	history.push_back(temp_history);
	
	//Alternance des vecteurs présent/futur.
	present = future;
	future = temp_ptr;
}

unsigned short int Universe::neighbors(unsigned int x, unsigned int y)
{
	unsigned short int count = 0;
	
	//Encadrement de la cellule en x.
	for (int i=-1 ; i <= 1 ; i++) {
		//Encadrement de la cellule en y.
		for (int j=-1 ; j <= 1 ; j++) {
			int pos_y = y+i;
			int pos_x = x+j;
			
			/*
			Le compilateur produira un warning "comparison between signed and unsigned integer expressions",
			normal puisque les positions relatives peuvent être négatives là où les dimensions ne le peuvent
			pas.
			*/
			if (limit == UniverseLimit::Bounded) {
				//On s'assure de ne pas être hors-limites.
				if (pos_y >= 0
				&& pos_y < this->y
				&& pos_x >= 0
				&& pos_x < this->x
				//Et on ignore la cellule elle-même.
				&& !(pos_x == x && pos_y == y))
				{
					if (present->at(pos_y).at(pos_x) == State::Alive)
					{
						count++;
					}
				}
			}
			// limit == UniversLimit::Continuous
			else {
				//On ignore la cellule elle-même.
				if (!(pos_x == x && pos_y == y)) {
					//On rectifie la position relative en position absolue.
					if (pos_x < 0) pos_x += this->x;
					else if (pos_x >= this->x) pos_x -= this->x;
					if (pos_y < 0) pos_y += this->y;
					else if (pos_y >= this->y) pos_y -= this->y;
					
					if (present->at(pos_y).at(pos_x) == State::Alive) count++;
				}
			}
		}
	}
	
	return count;
}

