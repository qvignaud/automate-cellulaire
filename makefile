
# La compilation dispose de l'option d'optimisation de niveau 3 (-O3) car elle améliore très significativement le temps d'exécution.

automate: main.o universe.o
	g++ -O3 -o automate main.o universe.o -std=c++11 -ltbb

main.o: main.cpp universe.h html_prev.xxd html_post.xxd
	g++ -O3 -o main.o -c main.cpp -W -Wall -pedantic -std=c++11

universe.o: universe.cpp
	g++ -O3 -o universe.o -c universe.cpp -W -Wall -pedantic -std=c++11
	
# Partie supérieure du fichier de visualisation.
html_prev.xxd: html_prev.html
	xxd -i html_prev.html html_prev.xxd

# Partie inférieure du fichier de visualisation.
html_post.xxd: html_post.html
	xxd -i html_post.html html_post.xxd

tests: automate
	@#Réutilisation de la même graine afin d'obtenir des univers initiaux identiques.
	echo $$$$ > /tmp/automate_random_seed
	@#Exécution en mode séquenciel et parallèle pour un univers borné.
	./automate -w 100 -h 100 -b b -s 50 -d 0 -m s -r $$(cat /tmp/automate_random_seed) -v /dev/null -o /tmp/automate_test_sequencial.txt
	./automate -w 100 -h 100 -b b -s 50 -d 0 -m m -r $$(cat /tmp/automate_random_seed) -v /dev/null -o /tmp/automate_test_multithread.txt
	@#Comparaison des résultats.
	diff -qs /tmp/automate_test_sequencial.txt /tmp/automate_test_multithread.txt
	@#Exécution en mode séquenciel et parallèle pour un univers continu.
	./automate -w 100 -h 100 -b c -s 50 -d 0 -m s -r $$(cat /tmp/automate_random_seed) -v /dev/null -o /tmp/automate_test_sequencial.txt
	./automate -w 100 -h 100 -b c -s 50 -d 0 -m m -r $$(cat /tmp/automate_random_seed) -v /dev/null -o /tmp/automate_test_multithread.txt
	@#Comparaison des résultats.
	diff -qs /tmp/automate_test_sequencial.txt /tmp/automate_test_multithread.txt

mesures: automate
	@echo "Mesures de l'automate cellulaire..."
	@echo -e "---\n\nMesures pour univers 10 x 10 pour 20 instants :"
	@./mesures.sh 10 10 20 5
	@echo -e "---\n\nMesures pour univers 20 x 10 pour 10 instants :"
	@./mesures.sh 20 10 10 5
	@echo -e "---\n\nMesures pour univers 10 x 20 pour 10 instants :"
	@./mesures.sh 10 20 10 5
	@echo -e "---\n\nMesures pour univers 50 x 50 pour 100 instants :"
	@./mesures.sh 50 50 100 5
	@echo -e "---\n\nMesures pour univers 50 x 150 pour 50 instants :"
	@./mesures.sh 50 150 50 5
	@echo -e "---\n\nMesures pour univers 500 x 500 pour 50 instants :"
	@./mesures.sh 500 500 50 5
	@echo -e "---\n\nMesures pour univers 1000 x 1000 pour 50 instants :"
	@./mesures.sh 1000 1000 50 5
	@echo -e "---\n\nMesures pour univers 1500 x 1500 pour 50 instants :"
	@./mesures.sh 1500 1500 50 5

mesures_csv: automate
	mkdir mesures
	@./mesures_csv.sh 10 10 20 5 mesures
	@./mesures_csv.sh 20 10 10 5 mesures
	@./mesures_csv.sh 10 20 10 5 mesures
	@./mesures_csv.sh 50 50 100 5 mesures
	@./mesures_csv.sh 50 150 50 5 mesures
	@./mesures_csv.sh 500 500 50 5 mesures
	@./mesures_csv.sh 1000 1000 50 5 mesures
	@./mesures_csv.sh 1500 1500 50 5 mesures

